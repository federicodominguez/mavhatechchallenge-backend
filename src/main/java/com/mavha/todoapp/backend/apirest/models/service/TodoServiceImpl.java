package com.mavha.todoapp.backend.apirest.models.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.mavha.todoapp.backend.apirest.exception.FileNotFound;
import com.mavha.todoapp.backend.apirest.models.dao.ITodoDao;
import com.mavha.todoapp.backend.apirest.models.entity.Status;
import com.mavha.todoapp.backend.apirest.models.entity.Todo;
import com.mavha.todoapp.backend.apirest.repository.SearchCriteria;
import com.mavha.todoapp.backend.apirest.repository.TodoSpecification;
import com.mavha.todoapp.backend.apirest.util.FileStorageProperties;

@Service
public class TodoServiceImpl implements ITodoService{
	
	private ITodoDao todoDao;
	private Path storagePath;
	
	@Autowired
	public TodoServiceImpl(ITodoDao todoDao,FileStorageProperties properties) {
		this.todoDao = todoDao;
		this.storagePath = Paths.get(properties.getUploadDir()).normalize();
		  if (Files.notExists(this.storagePath)) {
	            try {
	                Files.createDirectory(this.storagePath);
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }	
	}
	
	@Transactional(readOnly = true)
	public List<Todo> findByFilters(Long id, String description, Status status){
		List<SearchCriteria> params = new ArrayList<SearchCriteria>();
		if(id != null) {
			params.add(new SearchCriteria("id",":",id));
		}
		if(description != null) {
			params.add(new SearchCriteria("description",":",description));
		}
		if(status != null) {
			params.add(new SearchCriteria("status",":",status));
		}
		Specification <Todo> specs = getTodosFiltered(params);
		List<Todo> tds = (List<Todo>) this.todoDao.findAll(specs);
		return (List<Todo>) tds;	
	}
	
	@Transactional
	public Todo save(Todo todo, MultipartFile file) {
		
		if(file != null) {
			String fileName = UUID.randomUUID().toString() + "_" + file.getOriginalFilename().replace(" ","");
			Path target = Paths.get(storagePath.toString()).resolve(fileName).toAbsolutePath();
		
			try {
				Files.copy(file.getInputStream(), target, StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException ex) {
				try {
			       throw new FileNotFound("Error saving image.");
			    } catch (Exception e) {
			    	e.printStackTrace();
			    }
			}
			todo.setFile(fileName);
		}
		return todoDao.save(todo);
		
	}

	@Transactional(readOnly = true)
	public Todo findById(Long id) {
		return todoDao.findById(id).orElse(null);
	}

	@SuppressWarnings("unchecked")
	private Specification<Todo> getTodosFiltered(List<SearchCriteria> params){
		if(params.isEmpty()) {
			return null;
		}
		
		@SuppressWarnings("rawtypes")
		List<Specification> specs = params.stream()
				.map(TodoSpecification::new)
				.collect(Collectors.toList());
		
		@SuppressWarnings("rawtypes")
		Specification result = specs.get(0);
		
		for(int i = 1;i < params.size();i++) {
            result = Specification.where(result).and(specs.get(i));
		}

		return result;
	}
	
	@Transactional
	public void updateStatus(Long id, Status status) {
		this.todoDao.updateStatus(id, status);
	}
}