package com.mavha.todoapp.backend.apirest.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mavha.todoapp.backend.apirest.models.entity.Status;
import com.mavha.todoapp.backend.apirest.models.entity.Todo;
import com.mavha.todoapp.backend.apirest.models.service.ITodoService;

@CrossOrigin(origins= {"http://localhost:4200"})
@RestController
public class TodoRestController {
	
	private ITodoService todoService;
	
	@Autowired
	public TodoRestController(ITodoService todoService) {
		this.todoService = todoService;
	}

	//GET By filters
	@GetMapping("/todo")
	ResponseEntity<?> index(@RequestParam(value="id", required=false) Long id,
							@RequestParam(value="description", required=false) String description,
							@RequestParam(value="status", required=false) String status) {
		List<Todo> todos = null;
		Map<String,Object> response = new HashMap<String, Object>();
		Status statusTodo = null;
		if(status != null) {
			statusTodo = Status.PENDING.toString().equals(status) ? Status.PENDING : Status.DONE;
		}
		try {	
			todos = this.todoService.findByFilters(id,description,statusTodo);
		} catch (DataAccessException e) {
			response.put("message", "Error when querying the database");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(todos == null) {
			response.put("message", "There are no to do in the database");
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<Todo>>(todos,HttpStatus.OK);
	}
	
	//POST
	@PostMapping(path="/todo",consumes=MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> create(@RequestParam(value="description") String description,
            						@RequestParam(value="file", required=false) MultipartFile file){
		
		Todo todoNew = new Todo (description);
		Map<String, Object> response = new HashMap<String, Object>();
		try {
			this.todoService.save(todoNew, file);
		} catch(DataAccessException e) {
			response.put("message", "Error inserting into database");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
			
		response.put("message", "To do created successfully");
		response.put("todo", todoNew);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	//PUT update status
	@PutMapping("/todo/{id}")
	public ResponseEntity<?> updateStatus(@PathVariable Long id){
		
		Todo currentTodo = todoService.findById(id);
		Map<String, Object> response = new HashMap<String, Object>();
				
		if(currentTodo == null) {
			response.put("message", "Could not update the status of to do");
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.NOT_FOUND);
		}
	
		try {
			this.todoService.updateStatus(id, Status.DONE);
			currentTodo.setStatus(Status.DONE);
		} catch (DataAccessException e) {
			response.put("message", "Error updating database");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("message", "to do updated successfully");
		response.put("todo", currentTodo);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
		
	}
}