package com.mavha.todoapp.backend.apirest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TodoappBackendApirestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TodoappBackendApirestApplication.class, args);
	}

}
