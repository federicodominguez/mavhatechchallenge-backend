package com.mavha.todoapp.backend.apirest.models.entity;

public enum Status {
	PENDING,
    DONE
}
