package com.mavha.todoapp.backend.apirest.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mavha.todoapp.backend.apirest.models.entity.Status;
import com.mavha.todoapp.backend.apirest.models.entity.Todo;

@Repository
public interface ITodoDao extends JpaRepository<Todo, Long>, JpaSpecificationExecutor<Todo>{
	
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value = "UPDATE Todo t SET t.status = :status WHERE t.id = :id")
	void updateStatus(@Param("id") Long id,
					  @Param("status") Status status);

}