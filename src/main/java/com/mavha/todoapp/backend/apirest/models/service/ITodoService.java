package com.mavha.todoapp.backend.apirest.models.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.mavha.todoapp.backend.apirest.models.entity.Status;
import com.mavha.todoapp.backend.apirest.models.entity.Todo;

public interface ITodoService {
	
	public List<Todo> findByFilters(Long id, String description, Status status);

	public Todo save(Todo todo, MultipartFile files);

	public Todo findById(Long id);
	
	public void updateStatus(Long id, Status status);
	
}
