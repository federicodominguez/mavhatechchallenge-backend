# Back end

### Documentacion

Aplicacion construida utilizando Java con Spring Boot. La persistencia se logra utilizando JPA, con una base de datos H2
en memoria.

### Prerequisitos

  - Maven


## Instalacion
  
### 1. Run: 
    mvn install package
  
### 2. Luego, ejecutar el jar generado en la carpeta target: 
    java -jar nombre del jar

### Descripcion

TodoApp permite administrar un conjunto de tareas, donde cada To do se compone de un ID, una descripcion, imagen y un campo
de estado (PENDING, DONE). Es posible crear, leer o editar el estado de cada To do.

### API
- Endpoint de los metodos.

| Metodo | URL | Descripcion |
| ------ | ------ | ------ | 
| GET | /todo |  Retorna la lista de Todos almacenadas en la DB. Existe la posibilidad de filtrar por algunos campos | 
| POST | /todo | crea una nueva Todo en la DB |
| PUT | /todo/{id} | actualiza el estado de la Todo a partir su id |

### GET

Usando un método GET, el sistema devolverá una lista de todos almacenados en el sistema. El sistema puede filtrar los resultados si el usuario pasa los parámetros de consulta para eso. Ese proceso de filtrado podría hacerse por id, o / y descripción, o / y estado.

##### URL
* GET /todo

##### Parámetros de consulta
* id= Un identificador asociado a un Todo. Parámetro opcional
* description= Un conjunto de palabras o letras que debe tener una descripción de todo. Parámetro opcional
* status= Es el estado de tarea deseado para filtrar, que podría ser PENDING o DONE. Parámetro opcional

##### Respuesta exitosa
* Estado HTTP: 200 OK
* Ejemplo de contenido:
    {"id":1,"description":"ir al supermercado","status":"PENDING","file":"06b478f5-c509-46da-ac7d-c6e62dc0bac6_listadecompras.jpg"}
* Ejemplo filtrado por id:
    - GET /todo?id=1
    * {"id":1,"description":"ir al supermercado","status":"PENDING","file":"06b478f5-c509-46da-ac7d-c6e62dc0bac6_listadecompras.jpg"}
* Ejemplo filtrado por descripcion:
    - GET /todo?descripcion=ir+al+supermercado
    * {"id":1,"description":"ir al supermercado","status":"PENDING","file":"06b478f5-c509-46da-ac7d-c6e62dc0bac6_listadecompras.jpg"}
* Ejemplo filtrado por estado:
    - GET /todo?status=PENDING
    * {"id":1,"description":"ir al supermercado","status":"PENDING","file":"06b478f5-c509-46da-ac7d-c6e62dc0bac6_listadecompras.jpg"}

### POST

Mediante una solicitud POST, el usuario puede crear un nuevo Todo. El usuario debe dar una descripcion para crear una nueva, la imagen es opcional y se almacena en el sistema de archivos del servidor y esa ruta se almacena en el BD, almacenando la entidad Todo una referencia a esa ruta. Con esta estrategia, tenemos una mejor administración de memoria que si guardamos la imagen directamente en la base de datos como un campo Blob.

##### URL
* POST /todo

##### Parametros

* {descripcion, file}. file es opcional.
##### Ejemplo de contenido: 
* description = ir al supermercado image = @D:\Users\User\Documents\image\06b478f5-c509-46da-ac7d-c6e62dc0bac6_listadecompras.jpg
##### Respuesta exitosa
* Estado HTTP: 201 CREATED
* Contenido: 
{"todo":{"id":3,"description":"asd","status":"PENDING","file":"1ac6d42d-8d3a-49fa-b534-0a4ffe9c5f8b_listadecompras.jpg"},"message":"To do created successfully"}

### PUT
Usando una solicitud PUT, el usuario puede modificar un Todo existente. Solo puede modificar el estado de Todo, configurándolo en DONE.

##### URL
* PUT /todo/{id}

##### Parámetros de consulta
* id= Un identificador asociado a un todo. Parámetro obligatorio .

##### Respuesta exitosa
* Estado HTTP: 201 CREATED
* Contenido:
{"todo":{"id":1,"description":"ir al supermercado","status":"PENDING","file":"06b478f5-c509-46da-ac7d-c6e62dc0bac6_listadecompras.jpg"},"message":"to do updated successfully"}



